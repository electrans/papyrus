# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import papyrus


def register():
    Pool.register(
        papyrus.Document,
        papyrus.Page,
        module='electrans_papyrus', type_='model')
    Pool.register(
        papyrus.LotPapyrus,
        papyrus.ShipmentInternalPapyrus,
        papyrus.ShipmentOutPapyrus,
        papyrus.ShipmentInReturnPapyrus,
        module='electrans_papyrus', type_='report')

