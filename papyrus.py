# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, Workflow, fields
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.config import config as config_
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
import re

OUT_PREFIX = config_.get('papyrus', 'stock_shipment_out', default='ACL')
INTERNAL_PREFIX = config_.get('papyrus', 'stock_shipment_internal',
                              default='AIN')
DPR_PREFIX = config_.get('papyrus', 'stock_shipment_in_return', default='DPR')
LOT_PREFIX = config_.get('papyrus', 'stock_lot', default='LOT-')
COM_PREFIX = config_.get('papyrus', 'purchase_purchase', default='COM')
_SPLITTER = '%'


class Document(metaclass=PoolMeta):
    __name__ = 'papyrus.document'

    c_date = fields.Function(
        fields.Date("Create date"),
        'get_create_date')

    def get_record(self):
        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentInternal = pool.get('stock.shipment.internal')
        ShipmentInReturn = pool.get('stock.shipment.in.return')
        StockLot = pool.get('stock.lot')
        Purchase = pool.get('purchase.purchase')
        context = Transaction().context
        company_condition = ('company', '=', context.get("company", []))
        res = super().get_record()
        if res:
            return res
        if self.reference:
            if self.reference.startswith(OUT_PREFIX):
                number = self.reference
                _id = self.reference[len(OUT_PREFIX):]
                records = ShipmentOut.search([
                    ['OR',
                     ('id', '=', _id),
                     ('number', '=', number)],
                    company_condition],
                    limit=1)
                if records:
                    return records[0]
            if self.reference.startswith(INTERNAL_PREFIX):
                number = self.reference
                _id = self.reference[len(INTERNAL_PREFIX):]
                records = ShipmentInternal.search([
                    ['OR',
                     ('id', '=', _id),
                     ('number', '=', number)],
                    company_condition],
                    limit=1)
                if records:
                    return records[0]
            if self.reference.startswith(DPR_PREFIX):
                number = self.reference
                _id = self.reference[len(DPR_PREFIX):]
                records = ShipmentInReturn.search([
                    ['OR',
                     ('id', '=', _id),
                     ('number', '=', number)],
                    company_condition],
                    limit=1)
                if records:
                    return records[0]
            if self.reference.startswith(LOT_PREFIX):
                _id = self.reference[len(LOT_PREFIX):]
                records = StockLot.search([
                    ('id', '=', _id),
                    company_condition,
                ], limit=1)
                if records:
                    return records[0]
            if self.reference.startswith(COM_PREFIX):
                records = Purchase.search([
                    ('number', '=', self.reference),
                    company_condition,
                ], limit=1)
                if records:
                    return records[0]

    @classmethod
    @ModelView.button
    @Workflow.transition('inspected')
    def inspect(cls, documents):
        """
        Override inspect to use the DC-**** name file as reference
        """
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        # add condition to link the attachment to the purchase of right company
        # because there may be purchases with same number for different company
        context = Transaction().context
        company_condition = ('company', '=', context.get("company", []))
        # That pattern search "DC-" and optionally followed by "COM" then
        # 1 or more digits (part that we need) followed by a "_"
        # There will be 2 groups each one is specified with the ()
        # the second one is which we want
        pattern = r'DC(?:[_-]COM|[_-])?(\d+)[_-]'
        failed_documents = []
        for document in documents:
            match = re.search(pattern, document.filename)
            purchase = None
            if match:
                # Based on the filename, we can try to get the purchase
                # DC-12345_* or DC-COM12345_* or COMFORMITY111DC-12345_*
                # or COMFORMITY111DC-COM12345_* --> Will get 12345
                pos_purchase = match.group(1)
                purchase = Purchase.search([
                    ('number', '=', 'COM' + pos_purchase),
                    company_condition,
                ])
            if purchase:
                document.reference = purchase[0].number
            else:
                failed_documents.append(document)
        cls.email_inspection_failed(failed_documents) if failed_documents else None
        super(Document, cls).inspect(documents)

    @classmethod
    def email_inspection_failed(cls, documents):
        pool = Pool()
        Email = pool.get('ir.email')
        QualityConfiguration = pool.get('quality.configuration')
        quality_config = QualityConfiguration(1)
        AccountConfiguration = pool.get('account.configuration')
        account_config = AccountConfiguration(1)
        body = gettext('electrans_papyrus.document_inspection_failed_body',
                       documents=', '.join(set(d.filename.split(_SPLITTER)[-1]
                                               for d in documents)))
        files = []
        recipients = documents[0].filename.split(_SPLITTER)[0]
        recipients_cc = ', '.join(filter(None, [
            quality_config.quality_department_email.email if (
                quality_config.quality_department_email) else None,
            account_config.accounting_department_email.email if (
                account_config.accounting_department_email) else None
        ]))
        for d in documents:
            if d.filename and d.data:
                files.append((d.filename.split(_SPLITTER)[-1], d.data))
        # Need to specify the language context for cron jobs
        with Transaction().set_context(language='es'):
            Email.send(
                to=recipients,
                cc=recipients_cc,
                subject=gettext(
                    'electrans_papyrus.document_inspection_failed_subject',
                    doc=documents[0].filename.split(_SPLITTER)[-1]),
                body=body.replace("\n", "<br/>"),
                record=[cls.__name__, documents[0].id],
                files=files,
            )

    def scan(self):
        """
         Override scan to bypass the Scan process when the document is
         DC-**** because not needed
         """
        if not self.reference:
            return super(Document, self).scan()

    def get_attachment(self, record):
        """
        Override to keep the filename
        """
        old_name = None
        if self.filename:
            if (_SPLITTER in self.filename
                    and self.filename.split(_SPLITTER)[-1].startswith('DC-')):
                old_name = self.filename.split(_SPLITTER)[-1]
        attachment = super(Document, self).get_attachment(record)
        if old_name:
            attachment.name = old_name
        elif attachment.resource and getattr(attachment.resource, 'number') \
                and attachment.resource.number:
            attachment.name = attachment.resource.number + ".pdf"
        return attachment

    def get_create_date(self, name=None):
        return self.create_date

    def scan_engines(self):
        return ['datamatrix']


class Page(metaclass=PoolMeta):
    __name__ = 'papyrus.page'

    c_date = fields.Function(
        fields.Date("Create date"),
        'get_create_date')

    @classmethod
    def get_prefixes(cls):
        return super().get_prefixes() + [
            OUT_PREFIX, INTERNAL_PREFIX, LOT_PREFIX, DPR_PREFIX]

    def get_create_date(self, name=None):
        return self.create_date

    def scan_engines(self):
        return ['datamatrix']


class ShipmentInternalPapyrus(JasperReport):
    __name__ = 'stock.shipment.internal.papyrus'

    @classmethod
    def execute(cls, ids, data):
        parameters = {
            'prefix': INTERNAL_PREFIX,
        }
        if 'parameters' in data:
            data['parameters'].update(parameters)
        else:
            data['parameters'] = parameters
        return super().execute(ids, data)


class LotPapyrus(JasperReport):
    __name__ = 'stock.lot.papyrus'

    @classmethod
    def execute(cls, ids, data):
        parameters = {
            'prefix': LOT_PREFIX,
        }
        if 'parameters' in data:
            data['parameters'].update(parameters)
        else:
            data['parameters'] = parameters
        return super().execute(ids, data)


class ShipmentOutPapyrus(JasperReport):
    __name__ = 'stock.shipment.out.papyrus'

    @classmethod
    def execute(cls, ids, data):
        parameters = {
            'prefix': OUT_PREFIX,
        }
        if 'parameters' in data:
            data['parameters'].update(parameters)
        else:
            data['parameters'] = parameters
        return super().execute(ids, data)


class ShipmentInReturnPapyrus(JasperReport):
    __name__ = 'stock.shipment.in.return.papyrus'

    @classmethod
    def execute(cls, ids, data):
        parameters = {
            'prefix': DPR_PREFIX,
        }
        if 'parameters' in data:
            data['parameters'].update(parameters)
        else:
            data['parameters'] = parameters
        return super().execute(ids, data)
